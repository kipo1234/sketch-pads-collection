import sqlite from 'sqlite'
import SQL from 'sql-template-strings';

const initializeDatabase = async () => {

  const db = await sqlite.open('./db.sqlite');
  
  /**
   * retrieves the sketchpadsfrom the database
   */
  const getPadsList = async () => {
    const rows = await db.all("SELECT id AS id, type, used FROM sketchpads")
    return rows
  }
  const createContact = async (props) => {
    const { type, used } = props
    const result = await db.run(SQL`INSERT INTO sketchpads (type,used) VALUES (${type}, ${used})`);
    const id = result.stmt.lastID
    return id
  }


  const deleteContact = async (id) => {
    const result = await db.run(SQL`DELETE FROM sketchpads WHERE id = ${id}`);
    if(result.stmt.changes === 0){
      return false
    }
    return true
  }
  
  const updateContact = async (id, props) => {
    const { type, used } = props
    const result = await db.run(SQL`UPDATE sketchpads SET used=${used} WHERE id = ${id}`);
    if(result.stmt.changes === 0){
      return false
    }
    return true
  }

  const getContact = async (id) => {
    const contactsList = await db.all(SQL`SELECT id AS id, type, used FROM sketchpads WHERE id = ${id}`);
    const contact = contactsList[0]
    return contact
  }
  
  /**
   * retrieves the sketchpadsfrom the database
   * @param {string} orderBy an optional string that is either "type" or "used"
   * @returns {array} the list of contacts
   */
  const getContactsList = async (orderBy) => {
    let statement = `SELECT id AS id, type, used FROM contacts`
    switch(orderBy){
      case 'type': statement+= ` ORDER BY type`; break;
      case 'used': statement+= ` ORDER BY used`; break;
      default: break;
    }
    const rows = await db.all(statement)
    return rows
  }

  const controller = {
    createContact,
    deleteContact,
    updateContact,
    getContact,
    getContactsList,
    getPadsList
  }

  return controller


}

export default initializeDatabase
