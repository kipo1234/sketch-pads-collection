
      import app from './app'
      import initializeDatabase from './db'
      
      /** 
      const start = async () => {
        const controller = await initializeDatabase()
        app.get('/', (req, res) => res.send("ok"));
        
        app.get('/contacts/list', async (req, res) => {
          const contacts_list = await controller.getContactsList()
          res.json(contacts_list)
        })
        
        app.listen(8080, () => console.log('server listening on port 8080'))
      }
      */
      
      const start = async () => {
        const controller = await initializeDatabase()
        const id = await controller.createContact({type:"normal",used:"no"})
        const contact = await controller.getContact(id)
        console.log("------\nmy newly created contact\n",contact)
        // await controller.updateContact(id, {type:"Brad Pitt"})
        // await controller.updateContact(id, {used:"brad@pitt.com"})
        // const updated_contact = await controller.getContact(id)
        // console.log("------\nmy updated contact\n",updated_contact)
        // console.log("------\nlist of contacts before\n",await controller.getPadsList())
        // await controller.deleteContact(id)
        console.log("------\nlist of contacts after deleting\n",await controller.getPadsList())
        
        
      }
      start();
      