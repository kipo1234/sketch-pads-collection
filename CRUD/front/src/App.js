import React, { Component } from 'react';
import logo from './logo.svg';
import './App.css';

class App extends Component {
  
  state = { sketchpads:[] }
  async componentDidMount(){
    try{
        const response = await fetch('//localhost:8080/sketchpadscollection/list')
        const data = await response.json()
        this.setState({sketchpads:data})
    }catch(err){
      console.log(err)
    }
    }

  render() {
    const { sketchpads } = this.state
    return (
      <div className="App">
        <header className="App-header">
          <img src={logo} className="App-logo" alt="logo" />
          <p>
            Edit <code>src/App.js</code> and save to reload.
          </p>
          <a
            className="App-link"
            href="https://reactjs.org"
            target="_blank"
            rel="noopener noreferrer"
          >
            Learn React
          </a>
        </header>
        { sketchpads.map( contact => 
      <div key={contact.id}>
        <p>{contact.id} -  {contact.type}</p>
      </div>
  )
}

      </div>
    );
  }
}

export default App;
